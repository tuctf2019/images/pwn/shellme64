# shellme64 -- PWN -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/pwn/shellme64)

## Chal Info

Desc: `Same concept, more bytes`

Given files:

* shellme64

Hints:

* Just like x86 with some different registers.

Flag: `TUCTF{54m3_5h3llc0d3,_ju57_m0r3_by735}`

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/shellme64)

Ports: 8888

Example usage:

```bash
docker run -d -p 127.0.0.1:8888:8888 asciioverflow/shellme64:tuctf2019
```
