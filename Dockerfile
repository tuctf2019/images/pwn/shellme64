
FROM ubuntu:18.04

WORKDIR /usr/src/app

RUN apt -y update && apt -y upgrade
RUN apt -y install socat

RUN groupadd -g 999 chal && useradd -r -u 999 -g chal chal

COPY ./src .

RUN chown -R root:root /usr/src/app/

RUN chmod 444 /usr/src/app/flag.txt
RUN chmod 555 /usr/src/app/shellme64
RUN chmod 555 /usr/src/app

USER chal

CMD socat TCP-LISTEN:8888,fork,reuseaddr EXEC:"/usr/src/app/shellme64"
